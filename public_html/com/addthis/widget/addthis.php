<?php
/**
 * AddThis widget
 *
 * May 6, 2015
 *
 * @version 	0.1
 * @abstract
 * @package 	kernel
 * @author 		Translucide
 * @copyright 	copyright (c) 2012 Translucide
 * @licensea
 * @since 		0.1
 */

global $service;
$service->get('Ressource')->get('core/widget');
$service->get('Ressource')->get('com/addthis/locale/'.$service->get('Language')->getCode());

class AddthisWidget extends Widget{

	/**
	 * Information about this widget type
	 *
	 * @public
	 * @return array $info
	 */
	public function init(){
		$this->setInfo(array(
			'component' => 'addthis',
			'type' => 'block',
			'name' => 'addthis',
			'title' => ADDTHIS_WIDGET_TITLE,
			'description' => ADDTHIS_WIDGET_DESCRIPTION,
			'wireframe' => 'addthis',
			'icon' => 'addthis',
			'saveoptions' => array('jssource')
		));
	}
	
	public function render(){
		global $service;
		$opt = $this->data->getVar('widget_options');
		$service->get('Ressource')->getStyle('addthis','widget','addthis',$opt['stylesheet']);
		$id = 'widget_addthis_'.$this->data->getVar('widget_objid');
        $content = '<section id="'.$id.'" class="widget html addthis '.$opt['stylesheet'].' '.$this->data->getVar('widget_options')['widgetclasses'].'">';
		$content .= '<div class="ct"><div class="addthis_inline_share_toolbox"></div>';        
        if (trim($opt['jssource']) != '') {
            $js = str_replace(["<script type=\"text/javascript\" src=\"", "\"></script>"], "", $opt['jssource']);
            $service->get('Ressource')->get($js);
        }
		$content .= '</div></section>';
		return $content;
	}
	
	public function edit($objs,$form){
		global $service;
		$defaultlang = $service->get('Language')->getDefault();
		$store = new WidgetStore();
		$store->setOption('ignorelangs',true);
		$defobj = $store->getDefaultObj($objs);
		$options = $defobj->getVar('widget_options');				
		$form->add(new TextareaFormField('jssource',$options['jssource'],array(
			'tab'=> 'basic',
			'width' => 12,
            'rows' => '4',
			'title' => ADDTHIS_WIDGET_JSSOURCE
		)));		
		return $form;
	}
	
	public function hasAccess($op){
		global $service;		
		switch ($op) {
			case 'render' :{
				return true;
			}break;
			default:{
				if ($service->get('User')->isAdmin()) return true;
			}break;
		}
		return false;
	}	
	
}
?>